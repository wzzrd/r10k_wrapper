#!/bin/bash

PATH=/usr/bin:/usr/sbin

if [ -z "${1}" ]; then
  echo "Please pass an environment name as the first argument."
  exit 1
fi

environment="${1}"

if [ -e ~/.r10k_wrapper.conf ]; then
  source ~/.r10k_wrapper.conf
fi

if [ -z "${configuration_file}" ]; then
    configuration_file="/etc/puppet/r10k.yaml"
fi

if [ -z ${location} ] || [ -z ${organization} ]; then
  echo "Please setup an ~/.r10k_wrapper.conf file with:"
  echo "location=\"your location id\""
  echo "organization=\"your organization id\""
fi

echo "Working with environment: ${environment}"
echo "Location id is ${location}, organization id is ${organization}"
echo

hammer environment list | awk ' /^[0-9]/ { print $3 }' | grep -q ${environment}

if [ $? -eq 0 ]; then
  echo "Environment ${environment} already exists."
else
  echo "Environment ${environment} does not exist yet, creating..."
  hammer environment create --name ${environment} --location-ids ${location} --organization-ids ${organization}
fi

echo

echo "Pulling down modules..."
/usr/local/bin/r10k deploy environment ${environment} -v -p -c ${configuration_file}

echo
echo "Resetting ownership to apache:apache..."
chown -R apache:apache /etc/puppet/r10k/environments/${environment}

echo "Resetting SELinux context of environment ${environment}..."
restorecon -Fr /etc/puppet/r10k/environments/${environment}

echo
echo "Start the import of the environment in Satellite..."
hammer proxy import-classes --id 1 --environment ${environment}

if [ $? -eq 0 ]; then
    echo "Succesfully imported classes!"
else
    echo "Import failed!"
    exit 1
fi
